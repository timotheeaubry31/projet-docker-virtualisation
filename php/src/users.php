<?php
  // Se connecter à la base de données
  include("db_connect.php");
  $request_method = $_SERVER["REQUEST_METHOD"];

  // Recupere tous les users
  function getUsers()
  {
    global $conn;

    $query = "SELECT * FROM db.users";
    
    $response = array();
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result))
    {
      $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
  }

  // Recupere l'user {id}
  function getUser($id=0)
  {
    global $conn;

    $query = "SELECT * FROM db.users";

    if($id != 0)
    {
      $query .= " WHERE id=".$id." LIMIT 1";
    }
    $response = array();
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result))
    {
      $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
  }

  // Ajoute un user
  function AddUser()
  {
    global $conn;

    $firstname = $_POST["firstname"];
    $name = $_POST["name"];
    $email = $_POST["email"];
    $created = date('Y-m-d H:i:s');

    $query="INSERT INTO db.users(firstname, name, email, created) VALUES('".$firstname."', '".$name."', '".$email."', '".$created."')";

    if(mysqli_query($conn, $query))
    {
      $response=array(
        'status' => 1,
        'status_message' =>'User ajoute avec succes.'
      );
    }
    else
    {
      $response=array(
        'status' => 0,
        'status_message' =>'ERREUR!.'. mysqli_error($conn)
      );
    }
    echo json_encode($response);
  }

  // Modifie un user
  function updateUser($id)
  {
    global $conn;

    $_PUT = array();
    parse_str(file_get_contents('php://input'), $_PUT);
    $firstname = $_PUT["firstname"];
    $name = $_PUT["name"];
    $email = $_PUT["email"];

    $query="UPDATE db.users SET firstname='".$firstname."', name='".$name."', email='".$email."' WHERE id=".$id;
    
    if(mysqli_query($conn, $query))
    {
      $response=array(
        'status' => 1,
        'status_message' =>'User mis a jour avec succes.'
      );
    }
    else
    {
      $response=array(
        'status' => 0,
        'status_message' =>'Echec de la mise a jour de l\'user. '. mysqli_error($conn)
      );
      
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  // Supprime un user
  function deleteUser($id)
  {
    global $conn;
    $query = "DELETE FROM db.users WHERE id=".$id;
    if(mysqli_query($conn, $query))
    {
      $response=array(
        'status' => 1,
        'status_message' =>'User supprime avec succes.'
      );
    }
    else
    {
      $response=array(
        'status' => 0,
        'status_message' =>'La suppression de l\'user a echoue. '. mysqli_error($conn)
      );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  switch($request_method)
  {
    case 'GET':
      if(!empty($_GET["id"]))
      {
        // Récupérer un seul user
        $id = intval($_GET["id"]);
        getUser($id);
      }
      else
      {
        // Récupérer tous les users
        getUsers();
      }
    break;
    case 'POST':
        // Ajouter un user
        AddUser();
        break;
    case 'PUT':
        // Modifier un user
        $id = intval($_GET["id"]);
        updateUser($id);
        break;
    case 'DELETE':
        // Supprimer un user
        $id = intval($_GET["id"]);
        deleteUser($id);
        break;
    default:
      // Requête invalide
      header("HTTP/1.0 405 Method Not Allowed");
      break;
  }
?>