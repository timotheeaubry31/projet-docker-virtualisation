# REST API

The REST API to Create, Read, Update, Delete an user.

## Get all users

### Request

`GET http://localhost:8000/users`

## Get a specific user

### Request

`GET http://localhost:8000/users/:id`

## Create a new user

### Request

`POST http://localhost:8000/users`

### Parameters

3 parameters needed :
* firstname
* name
* email

## Update a user

### Request

`PUT http://localhost:8000/users/:id`

### Parameters

At least one of the three parameters :
* firstname
* name
* email

## Delete an user

### Request

`DELETE http://localhost:8000/users/:id`