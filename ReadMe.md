## Get started
***
* docker-compose build
* docker-compose up -d

## Project
***
* ./captures contains screenshots from postman using API requests
* ./php contains a Dockerfile and a folder ./php/src where are PHP files needed to build the REST API.
* ./docker-compose.yml

## The stack
***
The stack is made of :
* PHP 7.4 / Apache
* MySQL
* PhpMyAdmin